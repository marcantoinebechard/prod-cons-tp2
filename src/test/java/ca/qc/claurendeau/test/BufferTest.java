package ca.qc.claurendeau.test;

import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.*;

import org.junit.Test;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;
import ca.qc.claurendeau.storage.Buffer;
import ca.qc.claurendeau.storage.Element;

public class BufferTest {

    @Test
    public void bufferSizeTest() {
        Integer size = 1;
        Buffer buffer = new Buffer(size);
        assertEquals(1, buffer.capacity());
    }

    @Test(expected = BufferFullException.class)
    public void addElementBufferSizeZeroIsAlwaysFullTest() throws BufferFullException {
        Integer size = 0;
        Buffer buffer = new Buffer(size);
        Element element = new Element();
        buffer.addElement(element);
    }

    @Test
    public void testIsBufferEmptyWhenEmpty() {
        Integer size = 0;
        Buffer buffer = new Buffer(size);
        assertTrue(buffer.isEmpty());
    }

    @Test
    public void testIsBufferEmptyWhenNotEmpty() {
        Integer size = 1;
        Buffer buffer = new Buffer(size);
        Element element = new Element();
        buffer.setFirstElement(element);
        assertFalse(buffer.isEmpty());
    }

    @Test
    public void currentLoadZero() {
        Integer size = 3;
        Buffer buffer = new Buffer(size);
        assertEquals(0, buffer.getCurrentLoad());
    }

    @Test
    public void currentLoadOne() {
        Integer size = 3;
        Buffer buffer = new Buffer(size);
        Element element = new Element();
        buffer.setFirstElement(element);
        assertEquals(1, buffer.getCurrentLoad());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void negativeCapacityBuffer() {
        Integer capacity = -1;
        new Buffer(capacity);
    }

    @Test
    public void testIsBufferFullWhenNotFull() {
        Integer capacity = 1;
        Buffer buffer = new Buffer(capacity);
        assertFalse(buffer.isFull());
    }

    @Test
    public void testBufferIsFull() {
        Integer capacity = 1;
        Buffer buffer = new Buffer(capacity);
        Element element = new Element();
        buffer.setFirstElement(element);

        assertTrue(buffer.isFull());
    }

    @Test
    public void testBufferOfSizeZeroIsAlwaysFull() {
        Integer capacity = 0;
        Buffer buffer = new Buffer(capacity);
        assertTrue(buffer.isFull());
    }

    @Test
    public void testAddElementToEmptyBuffer() throws BufferFullException {
        Integer capacity = 2;
        Buffer buffer = new Buffer(capacity);
        Element element = new Element();
        buffer.addElement(element);

        assertTrue(buffer.getFirstElement() == element);
    }

    @Test
    public void testAddElementToNotFullBufferWithElements() throws BufferFullException {
        Integer capacity = 10;
        Buffer buffer = new Buffer(capacity);
        Element element = new Element(2);
        Element element2 = new Element(3);
        buffer.addElement(element);
        buffer.addElement(element2);

        assertTrue(buffer.getFirstElement().getNextElement() == element2);
    }

    @Test
    public void testGetCurrentLoadWithMoreThanOneElement() throws BufferFullException {
        Integer capacity = 10;
        Buffer buffer = new Buffer(capacity);
        Element element = new Element(2);
        Element element2 = new Element(3);
        Element element3 = new Element(0);
        Element element4 = new Element(90);
        
        buffer.addElement(element);
        buffer.addElement(element2);
        buffer.addElement(element3);
        buffer.addElement(element4);

        assertTrue(buffer.getCurrentLoad() == 4);
    }

    @Test(expected = BufferFullException.class)
    public void testAddToFullBuffer() throws BufferFullException {
        Integer capacity = 10;
        Buffer buffer = new Buffer(capacity);
        Element element = new Element(2);
        Element element2 = new Element(3);
        Element element3 = new Element(2);
        Element element4 = new Element(3);
        Element element5 = new Element(2);
        Element element6 = new Element(3);
        Element element7 = new Element(2);
        Element element8 = new Element(3);
        Element element9 = new Element(2);
        Element element10 = new Element(3);

        buffer.addElement(element);
        buffer.addElement(element2);
        buffer.addElement(element3);
        buffer.addElement(element4);
        buffer.addElement(element5);
        buffer.addElement(element6);
        buffer.addElement(element7);
        buffer.addElement(element8);
        buffer.addElement(element9);
        buffer.addElement(element10);

        assertTrue(buffer.isFull());

        Element element11 = new Element(10);
        buffer.addElement(element11);
    }

    @Test(timeout = 5000)
    public void getCurrentLoadOnCircularBuffer() throws BufferFullException {
        Integer capacity = 10;
        Buffer buffer = new Buffer(capacity);
        Element element = new Element(2);
        Element element2 = new Element(3);
        Element element3 = new Element(10);
        Element element4 = new Element(20);

        buffer.addElement(element);
        buffer.addElement(element2);
        buffer.addElement(element3);
        buffer.addElement(element4);
        buffer.addElement(element2);

        assertTrue(buffer.getCurrentLoad() == 4);
    }

    @Test
    public void testRemoveElementOnBufferWithOneElement() throws BufferFullException, BufferEmptyException {
        Integer capacity = 10;
        Buffer buffer = new Buffer(capacity);
        Integer valueOfDeletedElement = 2;
        Element element = new Element(valueOfDeletedElement);

        buffer.addElement(element);

        assertThat(buffer.removeElement().getData()).isEqualTo(valueOfDeletedElement);
        assertTrue(buffer.isEmpty());
    }

    @Test
    public void testGetLast() throws BufferFullException {
        Integer capacity = 10;
        Buffer buffer = new Buffer(capacity);
        Element element = new Element(2);
        Element element2 = new Element(3);
        Element element3 = new Element(3);
        Element element4 = new Element(3);

        buffer.addElement(element);
        buffer.addElement(element2);
        buffer.addElement(element3);
        buffer.addElement(element4);

        assertThat(buffer.getLast()).isEqualTo(element4);
    }

    @Test
    public void testGetLastOnCircularBuffer() throws BufferFullException {
        Integer capacity = 10;
        Buffer buffer = new Buffer(capacity);
        Element element = new Element(2);
        Element element2 = new Element(3);

        buffer.addElement(element);
        buffer.addElement(element2);
        buffer.addElement(element);

        assertThat(buffer.getLast()).isEqualTo(element2);
    }

    @Test
    public void testRemoveElementWithMoreThanOneElement() throws BufferFullException, BufferEmptyException {
        Integer capacity = 10;
        Buffer buffer = new Buffer(capacity);
        Element element = new Element(2);
        Element element2 = new Element(3);

        buffer.addElement(element);
        buffer.addElement(element2);

        assertThat(buffer.removeElement().getData()).isEqualTo(element2.getData());
        assertThat(buffer.getLast()).isEqualTo(element);
        assertTrue(buffer.getCurrentLoad() == 1);
    }

    @Test
    public void testGetOneBeforeLast() throws BufferFullException {
        Integer capacity = 10;
        Buffer buffer = new Buffer(capacity);
        Element element = new Element(2);
        Element element2 = new Element(3);
        Element element3 = new Element(4);
        Element element4 = new Element(5);
        Element element5 = new Element(6);

        buffer.addElement(element);
        buffer.addElement(element2);
        buffer.addElement(element3);
        buffer.addElement(element4);
        buffer.addElement(element5);

        assertThat(buffer.getOneBeforeLast()).isEqualTo(element4);
    }

    @Test
    public void testGetOneBeforeLastOneElementBuffer() throws BufferFullException {
        Integer capacity = 10;
        Buffer buffer = new Buffer(capacity);
        Element element = new Element(2);

        buffer.addElement(element);

        assertThat(buffer.getOneBeforeLast()).isEqualTo(element);
    }

    @Test
    public void testGetOneBeforeLastEmptytBuffer() throws BufferFullException {
        Integer capacity = 10;
        Buffer buffer = new Buffer(capacity);

        assertThat(buffer.getOneBeforeLast()).isEqualTo(null);
    }

    @Test(expected = BufferEmptyException.class)
    public void testRemoveElementEmptyBuffer() throws BufferEmptyException {
        Integer capacity = 10;
        Buffer buffer = new Buffer(capacity);

        buffer.removeElement();
    }

    @Test
    public void toStringBufferWithElements() throws BufferFullException {
        Integer capacity = 10;
        Buffer buffer = new Buffer(capacity);
        Element element = new Element(0);
        Element element2 = new Element(6);
        Element element3 = new Element(2);
        Element element4 = new Element(9);
        Element element5 = new Element(160);

        buffer.addElement(element);
        buffer.addElement(element2);
        buffer.addElement(element3);
        buffer.addElement(element4);
        buffer.addElement(element5);
        
        assertThat(buffer.toString()).isEqualTo(""+buffer.getCurrentLoad());

    }
}