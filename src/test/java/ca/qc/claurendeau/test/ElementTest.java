package ca.qc.claurendeau.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.qc.claurendeau.storage.Element;

public class ElementTest {

    @Test
    public void testGetElementData() {
        Integer data = 3;
        Element element = new Element(data);        
        assertTrue(data==element.getData());
    }

    @Test
    public void testSetNextElement(){
        Integer data = 3;
        Element element = new Element(data);
        Element element2 = new Element(data);
        element.setNextElement(element2);
    }
    
}
