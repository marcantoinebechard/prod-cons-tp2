package ca.qc.claurendeau.exception;

public class BufferEmptyException extends Exception {

    private static final long serialVersionUID = 2270250402666000188L;

    public BufferEmptyException(String message){
        super(message);
    }
}
