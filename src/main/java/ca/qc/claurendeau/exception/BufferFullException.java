package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {
    private static final long serialVersionUID = 2933212110488628480L;

    public BufferFullException(String message){
        super(message);
    }
}
