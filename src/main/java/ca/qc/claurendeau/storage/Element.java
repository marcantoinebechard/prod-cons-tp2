package ca.qc.claurendeau.storage;

public class Element {
    private Integer data;
    private Element nextElement;

    public Element() {
    }

    public Element(Integer data) {
        this.data = data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setNextElement(Element next) {
        this.nextElement = next;
    }

    public Element getNextElement() {
        return nextElement;
    }

    public int getData() {
        return data;
    }
    
    @Override
    public String toString(){
        return String.valueOf(data);
    }
}
