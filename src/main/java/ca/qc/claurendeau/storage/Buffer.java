package ca.qc.claurendeau.storage;

import java.util.ArrayList;
import java.util.List;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer {
    private int capacity;
    private Element firstElement;

    public Buffer(int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException("A buffer cannot have a negative capacity.");
        }
        this.capacity = capacity;
    }

    // returns the content of the buffer in form of a string
    public String toString() {
        String string = null;
        if(getCurrentLoad()>0){
            string = String.valueOf(getCurrentLoad());
        }
        return string;
    }

    public int capacity() {
        return this.capacity;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad() {
        if (firstElement != null) {
            return countElementsInBuffer();
        }
        return 0;
    }

    private Integer countElementsInBuffer() {
        Integer currentCount = 0;
        List<Element> alreadySeenElements = new ArrayList<>();
        Element currentElement = firstElement;
        alreadySeenElements.add(currentElement);
        currentCount++;
        while (currentElement.getNextElement() != null &&
                !alreadySeenElements.contains(currentElement.getNextElement())) {
            currentCount++;
            currentElement = currentElement.getNextElement();
            alreadySeenElements.add(currentElement);
        }
        return currentCount;
    }

    public boolean isEmpty() {
        return (this.firstElement == null);
    }

    public boolean isFull() {
        return (capacity <= getCurrentLoad());
    }

    public synchronized void addElement(Element element) throws BufferFullException {
        if (getCurrentLoad() == capacity) {
            throw new BufferFullException("Buffer is full.");
        }
        if (firstElement == null) {
            firstElement = element;
        } else {
            Element currentElement = firstElement;
            while (currentElement.getNextElement() != null) {
                currentElement = currentElement.getNextElement();
            }
            currentElement.setNextElement(element);
        }
    }

    public synchronized Element removeElement() throws BufferEmptyException {
        Element returnValue = null;
        if (firstElement == null) {
            throw new BufferEmptyException("Buffer is empty.");
        }

        if (firstElement.getNextElement() == null) {
            returnValue = new Element(firstElement.getData());
            firstElement = null;
        } else {
            Element beforeLast = getOneBeforeLast();
            returnValue = new Element(getLast().getData());
            beforeLast.setNextElement(null);
        }

        return returnValue;
    }

    public Element getLast() {
        Element currentElement = firstElement;
        List<Element> alreadySeenElements = new ArrayList<>();
        alreadySeenElements.add(currentElement);
        while (currentElement != null && currentElement.getNextElement() != null
                && !alreadySeenElements.contains(currentElement.getNextElement())) {
            currentElement = currentElement.getNextElement();
            alreadySeenElements.add(currentElement);
        }
        return currentElement;
    }

    public Element getOneBeforeLast() {
        Element currentElement = firstElement;
        Element last = getLast();
        if (last != null && firstElement != last) {
            while (currentElement.getNextElement() != last) {
                currentElement = currentElement.getNextElement();
            }
        }
        return currentElement;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setFirstElement(Element element) {
        this.firstElement = element;
    }

    public Element getFirstElement() {
        return firstElement;
    }
}
